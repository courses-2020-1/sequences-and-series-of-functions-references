# Sequences and series of functions references

References for the repository [sequences-and-series-of-functions](https://gitlab.com/courses-2020-1/sequences-and-series-of-functions) on GitLab.

<div align="center">
<h1>
    Sequences and series of functions
</h1>
</div>

A project about the sequences and series of functions $`f\colon D\times\mathbb{N}\to Z,\quad\left(x,n\right)\mapsto f_{n}\left(x\right)`$.

* **Deadline** :timer_clock: $`9-11`$ september $`2️020`$.
* Net estimated monograph extension: $`11`$ pages (length).

<p align="center">
    <img src="https://gitlab.com/courses-2020-1/sequences-and-series-of-functions/-/jobs/artifacts/master/raw/src/julia/test.svg?job=plot" alt="Sequence of functions"/>
    <img src="https://gitlab.com/courses-2020-1/sequences-and-series-of-functions/-/jobs/artifacts/master/raw/src/TeX/img/graph0.svg?job=build_pdf" alt="Diagram of relations between properties related to sequence of functions"/>
</p>

If you want to download the content of the repository, then you can choose the following [tarball](https://en.wikipedia.org/wiki/Tarball).

* [`sequences-and-series-of-functions-references-master.zip`](https://gitlab.com/courses-2020-1/sequences-and-series-of-functions-references/-/archive/master/sequences-and-series-of-functions-references-master.zip)
* [`sequences-and-series-of-functions-references-master.tar.gz`](https://gitlab.com/courses-2020-1/sequences-and-series-of-functions-references/-/archive/master/sequences-and-series-of-functions-references-master.tar.gz)
* [`sequences-and-series-of-functions-references-master.tar.bz2`](https://gitlab.com/courses-2020-1/sequences-and-series-of-functions-references/-/archive/master/sequences-and-series-of-functions-references-master.tar.bz2)
* [`sequences-and-series-of-functions-references-master.tar`](https://gitlab.com/courses-2020-1/sequences-and-series-of-functions-references/-/archive/master/sequences-and-series-of-functions-references-master.tar)
o maybe you execute the following instructions
```console
$> git clone --depth=1 https://gitlab.com/courses-2020-1/sequences-and-series-of-functions-references.git
$> cd sequences-and-series-of-functions-references
$> git lfs fetch origin master
```
and browse the `*.pdf`'s.

For more details, please see the [docs](https://docs.gitlab.com/ee/topics/git/lfs) for Git Large File Storage.