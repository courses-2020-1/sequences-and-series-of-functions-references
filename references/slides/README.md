## slides :woman_teacher: :man_teacher:

Main are slides are from 
* [Chi-Tsuen Yeh slides](http://yeh.nutn.edu.tw/calculus_adv/Rudin/ppt/index.php)
  - [`1_Number systems_2017.pdf`](http://yeh.nutn.edu.tw/calculus_adv/Rudin/ppt/01_Number_systems/1_Number%20systems_2017.pdf)
  - [`2a_Cardinality_2017.pdf`](http://yeh.nutn.edu.tw/calculus_adv/Rudin/ppt/02_Basic_topology/2a_Cardinality_2017.pdf)
  - [`2b_topology_2017.pdf`](http://yeh.nutn.edu.tw/calculus_adv/Rudin/ppt/02_Basic_topology/2b_topology_2017.pdf)
  - [`3a_compact_2017.pdf`](http://yeh.nutn.edu.tw/calculus_adv/Rudin/ppt/03_Compact/3a_compact_2017.pdf)
  - [`3b_PerfectSet_2017.pdf`](http://yeh.nutn.edu.tw/calculus_adv/Rudin/ppt/03_Compact/3b_PerfectSet_2017.pdf)
  - [`3c_ConnectedSet_2017.pdf`](http://yeh.nutn.edu.tw/calculus_adv/Rudin/ppt/03_Compact/3c_ConnectedSet_2017.pdf)
  - [`3a_Continuity_2017.pdf`](http://yeh.nutn.edu.tw/calculus_adv/Rudin/ppt/04_continuity/3a_Continuity_2017.pdf)
  - [`5a_Uniform-Convergence_2017.pdf`](http://yeh.nutn.edu.tw/calculus_adv/Rudin/ppt/05_Uniform_convergence/5a_Uniform-Convergence_2017.pdf)
  - [`6a_Approximations_Contractions.pdf`](http://yeh.nutn.edu.tw/calculus_adv/Rudin/ppt/06_Approximations_Contractions/6a_Approximations_Contractions.pdf)
  - [`7a_The-Riemann-Stieltjes-Integral.pdf`](http://yeh.nutn.edu.tw/calculus_adv/Rudin/ppt/07_The-Riemann-Stieltjes-Integral/7a_The-Riemann-Stieltjes-Integral.pdf)
* [Norbert Henze](https://www.math.kit.edu/stoch/~henze)
  - [`-asymptotische-stochastik-ws14-15.pdf`](https://www.math.kit.edu/stoch/~henze/media/-asymptotische-stochastik-ws14-15.pdf)
  - [`vorlesung_ss15.pdf`](https://www.math.kit.edu/stoch/~henze/media/vorlesung_ss15.pdf)
  - [`einf-stochastik-ws-2015-16.pdf`](https://www.math.kit.edu/stoch/~henze/media/einf-stochastik-ws-2015-16.pdf)

### View document in web browser :globe_with_meridians:

* [`1_Number systems_2017.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions-references-references/1_Number%20systems_2017.pdf)
* [`2a_Cardinality_2017.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions-references-references/2a_Cardinality_2017.pdf)
* [`2b_topology_2017.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions-references-references/2b_topology_2017.pdf)
* [`3a_Continuity_2017.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions-references-references/3a_Continuity_2017.pdf)
* [`3a_compact_2017.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions-references-references/3a_compact_2017.pdf)
* [`3b_PerfectSet_2017.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions-references-references/3b_PerfectSet_2017.pdf)
* [`3c_ConnectedSet_2017.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions-references-references/3c_ConnectedSet_2017.pdf)
* [`5a_Uniform-Convergence_2017.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions-references-references/5a_Uniform-Convergence_2017.pdf)
* [`6a_Approximations_Contractions.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions-references-references/6a_Approximations_Contractions.pdf)
* [`7a_The-Riemann-Stieltjes-Integral.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions-references-references/7a_The-Riemann-Stieltjes-Integral.pdf)
* [`-asymptotische-stochastik-ws14-15.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions-references-references/-asymptotische-stochastik-ws14-15.pdf)
* [`vorlesung_ss15.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions-references-references/vorlesung_ss15.pdf)
* [`einf-stochastik-ws-2015-16.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions-references-references/einf-stochastik-ws-2015-16.pdf)
