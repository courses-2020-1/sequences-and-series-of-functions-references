## extra

### Source

* [I Want to be a Mathematician](https://www.springer.com/gp/book/9780387960784)

### View document in web browser :globe_with_meridians:

* [I Want to be a Mathematician.pdf](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions/I%20Want%20to%20be%20a%20Mathematician.pdf)