# Funktionenfolge

Eine Funktionenfolge ist eine Folge, deren einzelne Glieder Funktionen
sind.
Funktionenfolgen und ihre Konvergenzeigenschaften sind für alle
Teilgebiete der Analysis von großer Bedeutung.
Vor allem wird hierbei untersucht, in welchem Sinne die Folge
konvergiert, ob die Grenzfunktion Eigenschaften der Folge erbt oder
ob Grenzwertbildungen bei Funktionenfolgen vertauscht werden können.
Zu den wichtigsten Beispielen zählen Reihen von Funktionen wie
Potenzreihen, Fourier-Reihen oder Dirichletreihen.
Hier spricht man auch von Funktionenreihen.

[![Mercator series](../../src/TeX/img/Mercator_series.svg "Mercator series")*Mercator series*][https://de.wikipedia.org/wiki/Funktionenfolge#/media/Datei:Mercator_series.svg]

<!-- blank line -->
<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/dwfU8l1QsKo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
<!-- blank line -->

- TOC
{:toc}

{::comment}
This is a comment which is
completely ignored.
{:/comment}

<!-- https://about.gitlab.com/handbook/markdown-guide/#table-of-contents-toc -->
https://docs.gitlab.com/ee/user/markdown.html
Text A
<!-- blank line -->
<br>
<!-- blank line -->
Text B

----

<details>
  <summary markdown="span">This is the summary text, click me to expand</summary>

  This is the detailed text.

  We can still use markdown, but we need to take the additional step of using the `parse_block_html` option as described in the [Mix HTML + Markdown Markup section](#mix-html--markdown-markup).

  You can learn more about expected usage of this approach in the [GitLab UI docs](https://gitlab-org.gitlab.io/gitlab-ui/?path=/story/base-collapse--default) though the solution we use above is specific to usage in markdown.
</details>

<details>
<summary markdown="span">First level collapsible item</summary>
**Lorem ipsum dolor sit amet...**
<details>
<summary markdown="span">Second level collapsible item</summary>
*Sed ut perspiciatis unde omnis iste natus...*
</details>
</details>

### GitLab Orange Heading
{: .gitlab-orange}

### GitLab Purple Heading
{: .gitlab-purple}

Center-aligned
{: .alert .alert-info .text-center}

My danger paragraph.
{: .alert .alert-danger}

## Definition

Eine (reelle) Funktionenfolge ist eine Folge
$`f_{1},f_{2},f_{3},\ldots`$ von Funktionen
$`f_{i}\colon\mathbb{R}\to\mathbb{R}`$.
Allgemeiner können Definitions- und Zielmenge auch andere Mengen
sein, beispielsweise Intervalle; sie müssen jedoch für alle
Funktionen dieselben sein.
Abstrakt kann eine Funktionenfolge als Abbildung
$$
f\colon D\times\mathbb{N}\to Z,\quad
\left(x,n\right)\mapsto f_{n}\left(x\right)
$$
für eine Definitionsmenge $`\boldsymbol{D}`$ und eine Zielmenge
$`\boldsymbol{Z}`$ definiert werden.
Falls als Indexmenge nicht die natürlichen Zahlen gewählt wurden, so
spricht man von einer Familie von Funktionen.

## Beispiele

### Vertauschung Grenzwert und Integralzeichen

Für die Folge $`\left(f_{n}\right)_{n\in\mathbb{N}},f_{n}:\left[0,2\right]\to\mathbb{R}`$
mit
$$
f_{n}\left(x\right)=
\begin{cases}
n^{2} x, & 0\leq x\leq\frac{1}{n} \\
2n-n^{2}x, & \frac{1}{n}\leq x\leq\frac{2}{n} \\
0, & x\geq\frac{2}{n}
\end{cases}
$$
gilt für jedes fixe $`\boldsymbol{x}`$
$$
\varliminf_{n\to\infty}f_{n}\left(x\right)=
0
$$
sie konvergiert punktweise gegen die Nullfunktion.
Jedoch gilt für alle $`\boldsymbol{n}\in\mathbb{N}`$
$$
\int_{0}^{2}f_{n}\left(x\right)\mathrm{d}x=
1
$$
also
$$
\lim_{n\to\infty}\int_{0}^{2}f_{n}\left(x\right)\mathrm{d}x\neq
\int_{0}^{2}\lim_{n\to\infty}f_{n}\left(x\right)\mathrm{d}x.
$$
Punktweise Konvergenz reicht also nicht aus, damit Grenzwert und
Integralzeichen vertauscht werden dürfen; damit diese Vertauschung
erlaubt ist, ist ein strengeres Konvergenzverhalten, typischerweise
gleichmäßige Konvergenz, majorisierte Konvergenz oder monotone Konvergenz,
hinreichend.

### Potenzreihen

In der Analysis treten Funktionenfolgen häufig als Summen von Funktionen,
also als Reihe auf, insbesondere als Potenzreihe oder allgemeiner als
Laurentreihe.

## Fourieranalyse und Approximationstheorie

In der Approximationstheorie wird untersucht, wie gut sich Funktionen
als Grenzwert von Funktionenfolgen darstellen lassen, wobei
insbesondere die quantitative Abschätzung des Fehlers von Interesse
ist.
Die Funktionenfolgen treten dabei üblicherweise als Funktionenreihen
auf, also als Summe $`\sum_{n=1}^{N}f_{n}\left(x\right)`$.

Beispielsweise konvergieren Fourierreihen im $`L^{2}`$-Sinn gegen die
darzustellende Funktion.
Bessere Approximationen im Sinne der gleichmäßigen Konvergenz erhält
man oft mit Reihen aus Tschebyschow-Polynomen.

## Stochastik

In der Stochastik ist eine Zufallsvariable $X$ als messbare Funktion
$`X\colon\Omega\to\mathbb{R}`$ eines Maßraums $`\left(\Omega,\Sigma,P\right)`$
mit einem Wahrscheinlichkeitsmaß $`P\left(\Omega\right)=1`$ definiert.
Folgen $`X_{n}`$ von Zufallsvariablen sind daher spezielle Funktionenfolgen,
ebenso sind Statistiken wie z. B. der Stichprobenmittelwert
$`\overline{X}_{N}\coloneqq\frac{1}{n}\sum_{n=1}^{N}X_{n}`$ Funktionenfolgen.
Wichtige Konvergenzeigenschaften dieser Funktionenfolgen sind z. B.
das starke Gesetze der großen Zahlen und das schwache Gesetz der
großen Zahlen.

## Numerische Mathematik

In der numerischen Mathematik tauchen Funktionenfolgen beispielsweise
bei der Lösung von partiellen Differentialgleichungen $`Df=0`$ auf,
wobei $`D`$ ein (nicht notwendigerweise linearer) Differentialoperator
und die gesuchte Funktion ist.
Bei der numerischen Lösung etwa mit der finiten Elementmethode erhält
man Funktionen $`f_{n}`$ als Lösung der diskretisierten Version der
Gleichung, wobei die Feinheit der Diskretisierung bezeichnet.
Bei der Analyse des numerischen Algorithmus werden nun die
Eigenschaften der diskretisierten Lösungen $`f_{n}`$, die eine
Funktionenfolge bilden, untersucht; insbesondere ist es sinnvoll,
dass die Folge der diskretisierten Lösungen $`f_{n}`$ bei Verfeinerung der Diskretisierung gegen
die Lösung des Ausgangsproblems konvergiert.

## Eigenschaften

### Monotonie
→ Hauptartikel: Monotone Funktionenfolge

Eine Funktionenfolge $`{\left(f_{i}\right)}_{i\in\mathbb{N}}`$ heißt
monoton wachsend (monoton fallend) auf $`D`$, wenn $`f_{i}\left(x\right)\leq f_{i+1}\left(x\right)`$
($`f_{i}\left(x\right)\geq f_{i+1}\left(x\right)`$) für alle $`x\in D`$ ist.
Sie heißt monoton, wenn sie entweder monoton fallend oder monoton wachsend ist.

### Punktweise Beschränktheit

Eine Funktionenfolge $`{\left(f_{i}\right)}_{i\in\mathbb{N}}`$ auf
einer Menge $D$, deren Wertevorrat ein normierter Raum ist, heißt
punktweise beschränkt, wenn für jeden Punkt $x\in D$ die Menge
$`\left\{f_{i}\left(x\right)\mid i\in\mathbb{N}\right\}`$ beschränkt ist.
Diese Menge ist also die Menge aller Werte, die an der Stelle $`x`$ von einer Funktion der Folge angenommen wird.

### Gleichmäßige Beschränktheit
Eine Funktionenfolge $`f_{i}\colon D\to\mathbb{R},i\in\mathbb{N}`$
ist auf einer Menge $`A\subset D`$ gleichmäßig beschränkt, falls
eine Konstante $`c\in\mathbb{R}`$ existiert, so dass
$`\left|f_{i}\left(x\right)\right|\leq c`$  für alle $`i\in\mathbb{N}`$
und alle $`x\in A`$.

Eine Funktionenfolge kann also höchstens dann gleichmäßig beschränkt
sein, wenn jede einzelne Funktion der Folge beschränkt ist.
Für jede einzelne Funktion $`f_{i}$`$ existiert daher die
Supremumsnorm $`{\left\|f_{i}\right\|}_{\infty}=\sup\left\{\left|f_{i}\left(x\right)\right|:x\in X\right\}`$.
Eine Funktionenfolge ist nun genau dann gleichmäßig beschränkt, wenn
sie als Menge von Funktionen bezüglich der Supremumsnorm beschränkt
ist.

Dies wird auf vektorwertige Funktionen verallgemeinert: Dabei ist $`D`$
eine beliebige Menge $`Z`$, ein reeller oder komplexer normierter Raum
mit der Norm $`{\left\|\cdot\right\|}_{Z}\colon Z\to\mathbb{R}^{}+`$.
Man bezeichnet die Menge der auf $`D`$ definierten
Funktionen, die bezüglich der Norm in $`Z`$ beschränkt sind, als
$`B\left(D\right)`$ und führt auf $`B\left(D\right)`$ mit
$`{\left\|f\right\|}_{\infty}=\sup\left\{{\left\|f\left(x\right)\right\|}_{Z}:x\in D\right\}`$.
eine Norm ein, die $`B\left(D\right)`$ wiederum zu einem normierten Raum macht.
Dann ist eine Funktionenfolge mit auf $`D`$ definierten Funktionen
genau dann gleichmäßig beschränkt, wenn die Folge eine Teilmenge von
$`B\left(D\right)`$ ist und als Teilmenge von $`\left(B\left(D\right),{\left\|\cdot\right\|}_{\infty}\right)`$
beschränkt ist.

Eine gleichmäßig beschränkte Funktionenfolge ist notwendigerweise
auch punktweise beschränkt.

### Lokal gleichmäßige Beschränktheit
Eine Funktionenfolge $`f_{i}\colon D\to\mathbb{R}, i\in\mathbb{N}`$
ist auf einer offenen Menge $`A\subset D`$ lokal gleichmäßig
beschränkt, falls zu jedem $`x_{0}\in A`$ eine offene Umgebung
$`U\left(x_{0}\right)`$ und eine Konstante $`c\in\mathbb{R}`$
existiert, so dass $`\left|f_{i}\left(x\right)\right|\leq c`$
gilt für alle $`i\in\mathbb{N}`$ und alle $`x\in U\left(x_0\right)`$.

## Konvergenzbegriffe

Der Grenzwert $`f`$ einer Funktionenfolge wird *Grenzfunktion* genannt.
Da die in den Anwendungen auftretenden Funktionsfolgen sehr
unterschiedliches Verhalten bei wachsendem Index haben können, ist es
notwendig, sehr viele verschiedene Konvergenzbegriffe für
Funktionenfolgen einzuführen.
Von einem abstrakteren Standpunkt handelt es sich meist um die
Konvergenz bezüglich gewisser Normen oder allgemeiner Topologien auf
den entsprechenden Funktionenräumen; vereinzelt treten aber auch
andere Konvergenzbegriffe auf.

Die verschiedenen Konvergenzbegriffe unterscheiden sich vor allem
durch die implizierten Eigenschaften der Grenzfunktion.
Die wichtigsten sind:

### Klassische Konvergenzbegriffe

#### Punktweise Konvergenz

Existiert der punktweise Grenzwert

$$
f\left(x\right)=
\lim_{n\to\infty}f_{n}\left(x\right)
$$
in jedem Punkt $`\boldsymbol{x}`$ des Definitionsbereiches, so wird
die Funktionenfolge punktweise konvergent genannt.
Beispielsweise gilt
$$
\varliminf_{n\to\infty}\cos^{2n}x=
\begin{cases}
1, & x=\pi k,k\in\mathbb{Z} \\
0, & \text { sonst }
\end{cases}
$$
die Grenzfunktion ist also unstetig.

#### Gleichmäßige Konvergenz
Eine Funktionenfolge $`\left(f_{n}\right)_{n}`$ ist gleichmäßig
konvergent gegen eine Funktion $`f`$, wenn die maximalen Unterschiede
zwischen $`f_{n}`$ und $`f`$ gegen null konvergieren.
Dieser Konvergenzbegriff ist Konvergenz im Sinne der Supremumsnorm.

Gleichmäßige Konvergenz impliziert einige Eigenschaften der
Grenzfunktion, wenn die Folgenglieder sie besitzen:

* Der gleichmäßige limes stetiger Funktionen ist stetig.
* Der gleichmäßige Limes einer Folge (Riemann- bzw. Lebesgue-)
integrierbarer Funktionen auf einem kompakten Intervall ist
(Riemann- bzw. Lebesgue-)integrierbar, und das Integral der
Grenzfunktion ist der limes der Integrale der Folgenglieder:
Ist $`\left(f_{n}\right)_{n}`$ gleichmäßig konvergent gegen $`f`$, so gilt
$$
\lim_{n\to\infty}\int_{a}^{b}f_{n}=
\int_{a}^{b}f
$$
* Konvergiert eine Folge $`\left(f_{n}\right)_{n}`$ differenzierbarer
Funktionen punktweise gegen eine Funktion $f$ und ist die Folge der Ableitungen gleichmäßig konvergent, so ist $f$ differenzierbar und es gilt
$$
\lim _{n\to\infty}f_{n}^{\prime}=
f^{\prime}
$$

#### Lokal gleichmäßige Konvergenz

Viele Reihen in der Funktionentheorie, insbesondere Potenzreihen,
sind nicht gleichmäßig konvergent, weil die Konvergenz für zunehmende
Argumente immer schlechter wird. Verlangt man die gleichmäßige
Konvergenz nur lokal, das heißt in einer Umgebung eines jeden
Punktes, so kommt man zum Begriff der lokal gleichmäßigen Konvergenz,
der für viele Anwendungen in der Analysis ausreicht.
Wie bei der gleichmäßigen Konvergenz überträgt sich auch bei lokal
gleichmäßiger Konvergenz die Stetigkeit der Folgenglieder auf die
Grenzfunktion.

#### Kompakte Konvergenz

Ein ähnlich guter Konvergenzbegriff ist der der kompakten Konvergenz,
der gleichmäßige Konvergenz lediglich auf kompakten Teilmengen fordert.
Aus der lokal gleichmäßigen Konvergenz folgt die kompakte Konvergenz;
für lokalkompakte Räume, die häufig in Anwendungen auftreten, gilt
die Umkehrung.

#### Normale Konvergenz

In der Mathematik dient der Begriff der normalen Konvergenz der
Charakterisierung von unendlichen Reihen von Funktionen.
Eingeführt wurde der Begriff von dem französischen Mathematiker René
Louis Baire.

### Maßtheoretische Konvergenzbegriffe
Bei den maßtheoretischen Konvergenzbegriffen ist die Grenzfunktion
üblicherweise nicht eindeutig, sondern nur fast überall eindeutig
definiert.
Alternativ lässt sich diese Konvergenz auch als Konvergenz von
Äquivalenzklassen von Funktionen, die fast überall übereinstimmen,
auffassen.
Als eine solche Äquivalenzklasse ist dann der Grenzwert eindeutig
bestimmt.

#### Punktweise Konvergenz fast überall
→ Hauptartikel: Punktweise Konvergenz μ-fast überall

Sind ein Maßraum $`\left(\Omega,\Sigma,\mu\right)`$ und eine Folge
darauf messbarer Funktionen $`f_{n}`$ mit Definitionsmenge $`\Omega`$
gegeben, so wird die Funktionenfolge punktweise konvergent fast
überall bezüglich $`\mu`$ genannt, wenn der punktweise Grenzwert
$$
f\left(x\right)=\lim_{n\to\infty}f_{n}\left(x\right)
$$
fast überall bezüglich $`\mu`$ existiert, wenn also eine Menge
$`Z\in\Sigma`$ vom Maß Null $`(\mu\left(Z\right)=0)`$ existiert,
sodass $`f_{n}`$ eingeschränkt auf das Komplement $`\Omega\setminus Z`$
punktweise konvergiert.

Die Konvergenz fast überall bezüglich eines Wahrscheinlichkeitsmaßes
wird in der Stochastik fast sichere Konvergenz genannt.

Beispielsweise gilt

$`\lim_{n\to\infty}\cos^{2n}x=0`$ punktweise fast überall bezüglich des Lebesgue-Maßess.

Ein anderes Beispiel ist die Funktionenfolge
$`f_{n}\colon\left[0,1\right]\to\left[0,1\right]`$, wobei für $`n=2^{r}+s`$, $`0\leq s\leq 2^{r}-1`$
$$
f_{2^{r}+s}\left(x\right)\coloneqq
\begin{cases}
1, & \frac{s}{2^{r}}\leq x\leq\frac{s+1}{2^{r}} \\
0, & \text { sonst. }
\end{cases}
$$
Diese Folge konvergiert für kein $`x\in\left[0,1\right]`$, da sie für
jedes fixe $`x`$ die Werte $`0`$ und $`1`$ unendlich oft annimmt.
Für jede Teilfolge $`f_{n_{k}},k\in\mathbb{N}`$ lässt sich aber eine
Teilteilfolge $`f_{n_{k_{l}}},l\in\mathbb{N}`$ angegeben, sodass

$\lim_{l\to\infty}f_{n_{k_{l}}}\left(x\right)=0$ punktweise fast überall bezüglich des Lebesgue-Maßes.

Gäbe es eine Topologie der punktweisen Konvergenz fast überall, so
würde daraus, dass jede Teilfolge von $f_{n}$ eine Teilteilfolge
enthält, die gegen $`0`$ konvergiert, folgen, dass $`f_{n}`$ gegen
$`0`$ konvergieren muss.
Da aber $`f_{n}`$ nicht konvergiert, kann es folglich keine Topologie
der Konvergenz fast überall geben.
Die punktweise Konvergenz fast überall ist damit ein Beispiel eines
Konvergenzbegriffes, der zwar den Fréchet-Axiomen genügt, aber nicht
durch eine Topologie erzeugt werden kann.

#### Konvergenz dem Maße nach
→ Hauptartikel: Konvergenz nach Maß und Konvergenz lokal nach Maß

In einem Maßraum $`\left(\Omega,\Sigma,\mu\right)`$ wird eine Folge
darauf messbarer Funktionen $`f_{n}`$ *konvergent dem Maße nach*
gegen eine Funktion $`\boldsymbol{f}`$ genannt, wenn für jedes
$`\boldsymbol{\varepsilon}>\mathbf{0}`$
$$
\lim_{n\to\infty}
\mu\left(\left\{x:\left|f_{n}\left(x\right)-f\left(x\right)\right|\geq\varepsilon\right\}\right)=
0
$$
gilt.

In einem endlichen Maßraum, also wenn $`\mu\left(\Omega\right)<\infty`$
gilt, ist die Konvergenz dem Maße nach schwächer als die Konvergenz
fast überall: Konvergiert eine Folge messbarer Funktionen $`f_{n}`$
fast überall gegen Funktion $\boldsymbol{f}$, so konvergiert sie auch
dem Maße nach gegen $`f`$.[3]

In der Stochastik wird die Konvergenz dem Maße nach als Stochastische
Konvergenz oder als *Konvergenz in Wahrscheinlichkeit* bezeichnet.

Eine Abschwächung der Konvergenz dem Maße nach ist die Konvergenz
lokal nach Maß.
Auf endlichen Maßräumen stimmen beide Begriffe überein.

#### $`L^{p}`$ -Konvergenz und Konvergenz in Sobolew-Räumen
Hauptartikel: Konvergenz im p-ten Mittel

Eine Funktionenfolge $`f_{n}`$ heißt $`L^{p}`$ konvergent gegen $`f`$ oder
konvergent im p-ten Mittel, wenn sie im sinne des entsprechenden
$`L^{p}`$-Raums $\mathcal{L}^{p}\left(\Omega,\mathcal{A},\mu;E\right)$
konvergiert, wenn also
$$
\lim_{n\rightarrow\infty}
{\left\|f_{n}-f\right\|}_{p}=
\lim_{n\to\infty}{\left(\int_{\Omega}{\left\|f_{n}\left(x\right)-f\left(x\right)\right\|}^{p}\mathrm{d}\mu\left(x\right)\right)}^{1/p}=
0.
$$
Ist $`\mu`$ ein endliches Maß, gilt also $`\mu(\Omega)<\infty`$, so
folgt für $`q\geq p\geq0`$ aus der Ungleichung der verallgemeinerten
Mittelwerte, dass eine Konstante $k\in\mathbb{R}^{+}$ existiert,
sodass ${\left\|f\right\|}_{p}\leq k{\left|f\right\|}_{q}$;
insbesondere folgt dann also aus der $`L^{q}`$-Konvergenz von
$`f_{n}`$ gegen $`f`$ auch die $`L^{p}`$-Konvergenz von $`f_{n}`$
gegen $`f`$.


Aus der $`L^{p}`$-Konvergenz folgt die Konvergenz dem Maße nach, wie
man aus der Tschebyschow-Ungleichung in der Form
$$
\mu\left\{x:\left|f_{n}\left(x\right)-f\left(x\right)\right|\geq\varepsilon\right\}\leq
\frac{1}{\varepsilon^{p}}\int_{\Omega}{\left|f_{n}\left(x\right)-f\left(x\right)\right|}^{p}\mathrm{d}\mu(x)
$$
sieht.[5]

Eine Verallgemeinerung der $`L^{p}`$-Konvergenz ist die Konvergenz in
Sobolew-Räumen, die nicht nur die Konvergenz der Funktionswerte,
sondern auch die Konvergenz gewisser Ableitungen berücksichtigt.
Der Sobolewschen Einbettungssatz beschreibt die Abhängigkeiten der
Konvergenzbegriffe in den unterschiedlichen Sobolew-Räumen.

#### Fast gleichmäßige Konvergenz
→ Hauptartikel: Fast gleichmäßige Konvergenz
In einem Maßraum $`\left(\Omega,\Sigma,\mu\right)`$ wird eine Folge
darauf messbarer reell- oder komplexwertiger Funktionen $`f_{n}`$
fast gleichmäßig konvergent gegen eine Funktion $`f`$ genannt, wenn
für jedes $`\varepsilon>0`$ eine Menge $`A\in\Sigma`$ existiert,
sodass $`\mu\left(A\right)<\varepsilon`$ und $`f_{n}`$ auf dem
Komplement $`\Omega\setminus A`$ gleichmäßig gegen $`f`$ konvergiert. [6]

Aus der fast gleichmäßigen Konvergenz folgt die punktweise Konvergenz
fast überall [7]; aus dem Satz von Jegorow folgt, dass in einem
endlichen Maßraum auch umgekehrt aus der punktweisen Konvergenz fast
überall die fast gleichmäßige Konvergenz folgt. [8]
In einem endlichen Maßraum, also insbesondere für reellwertige
Zufallsvariablen, sind Konvergenz fast überall und fast gleichmäßige
Konvergenz von reellwertigen Funktionenfolgen äquivalent.
Aus der fast gleichmäßigen Konvergenz folgt außerdem die Konvergenz
dem Maße nach [7] . Umgekehrt gilt,
dass eine dem Maße nach konvergente Folge eine Teilfolge enthält, die
fast gleichmäßig (und damit auch fast überall) gegen die gleiche
Grenzfolge konvergiert. [9]

#### Fast überall gleichmäßige Konvergenz
→ Hauptartikel: Gleichmäßige Konvergenz $`\mu`$-fast überall

In einem Maßraum $`\left(\Omega,\Sigma,\mu\right)`$ wird eine Folge
darauf messbarer reell- oder komplexwertiger Funktionen $`f_{n}`$
fast überall gleichmäßig konvergent gegen eine Funktion $`f`$ genannt,
wenn es eine Nullmenge `$Z\in\Sigma$` gibt, sodass $`f_{n}`$ auf dem
Komplement $`\Omega\setminus Z`$ gleichmäßig gegen $`f`$ konvergiert.
Für Folgen beschränkter Funktionen ist das im Wesentlichen die
Konvergenz im Raum $`L^{\infty}\left(\Omega,\Sigma,\mu\right)`$.
Fast überall gleichmäßige Konvergenz kann wegen der sehr ähnlichen
Bezeichnung leicht mit fast gleichmäßiger Konvergenz verwechselt
werden, wie Paul Halmos in seinem Lehrbuch zur Maßtheorie kritisiert. [10]

#### Schwache Konvergenz
Hauptartikel: Schwache Konvergenz in $`Lp`$

Die schwache Konvergenz für Funktionenfolgen ist ein Spezialfall der
schwachen Konvergenz im Sinne der Funktionalanalysis, die allgemein
für normierte Räume definiert wird.
Zu beachten ist, dass es in der Funktionalanalysis, der Maßtheorie
und der Stochastik mehrere verschiedene Konzepte von schwacher
Konvergenz gibt, die nicht miteinander verwechselt werden sollten.

Für $`\boldsymbol{p}\in[1,\infty)`$ heißt eine Funktionenfolge
$`\left(f_{n}\right)_{n\in\mathbb{N}}`$ aus $`\mathcal{L}^{p}`$
schwach konvergent gegen $`f`$, wenn für alle
$`\boldsymbol{g}\in\mathcal{L}^{q}`$ gilt, dass
$$
\lim_{n\to\infty}\int_{X}f_{n}g\mathrm{d}\mu=
\int_{X}fg\mathrm{d}\mu
$$
ist.
Dabei ist $`\boldsymbol{q}`$ durch
$`\frac{1}{\boldsymbol{p}}+\frac{1}{\boldsymbol{q}}=1`$ definiert.

#### Übersicht über die maßtheoretischen Konvergenzarten

Die nebenstehende Übersicht entstammt dem Lehrbuch
*Einführung in die Maßtheorie* von Ernst Henze, der dafür seinerseits
auf ältere Vorgänger verweist.[11]
Sie verdeutlicht die logischen Beziehungen zwischen den
Konvergenzarten für eine Folge messbarer Funktionen auf einem Maßraum
$`\left(\Omega,\Sigma,\mu\right)$.
Ein schwarzer, durchgehender Pfeil bedeutet, dass die Konvergenzart
an der Pfeilspitze aus der Konvergenzart am Pfeilursprung folgt.
Für die blauen gestrichelten Pfeile gilt dies nur, wenn
$`\mu\left(\Omega\right)<\infty`$ vorausgesetzt ist.
Für die roten Strichpunktpfeile gilt die Implikation, wenn die Folge
durch eine $`\mu`$-integrierbare Funktion beschränkt ist.

## Hierarchische Ordnung Konvergenzbegriffe in Räumen

In Maßräumen $`\left(\Omega,\Sigma,\mu\right)`$ mit endlichem Maß,
wenn also $`\mu\left(\Omega\right)<\infty`$ gilt, ist es großteils
möglich, die unterschiedlichen Konvergenzbegriffe nach ihrer Stärke
zu ordnen.
Dies gilt insbesondere in Wahrscheinlichkeitsräumen, da dort ja
$`\mu\left(\Omega\right)=1`$ gilt.

Aus der gleichmäßigen Konvergenz folgt die Konvergenz dem Maße nach
auf zwei unterschiedlichen Wegen, der eine führt über die punktweise
Konvergenz:

* $`\boldsymbol{f}_{n}\to\boldsymbol{f}`$ gleichmäßig
$`\implies\boldsymbol{f}_{n}\to\boldsymbol{f}$ lokal gleichmäßig (d. h.
gleichmäßig auf einer Umgebung eines jeden Punktes).
* $`\boldsymbol{f}_{n}\to\boldsymbol{f}`$ lokal gleichmäßig
$`\implies\boldsymbol{f}_{n}\to\boldsymbol{f}`$ kompakt (d. h.
gleichmäßig auf jeder kompakten Teilmenge).
* $`\boldsymbol{f}_{n}\to\boldsymbol{f}`$ kompakt
$`\implies\boldsymbol{f}_{n}\to\boldsymbol{f}`$ punktweise (jeder
einzelne Punkt ist ja eine kompakte Teilmenge).
* $`\boldsymbol{f}_{n}\to\boldsymbol{f}`$ punktweise
$`\implies\boldsymbol{f}_{n}\to\boldsymbol{f}`$ punktweise fast
überall (bzw. fast sicher).
* $`f_{n}\to f`$ punktweise fast überall $`\iff f_{n}\to f`$ fast gleichmäßig.
* $f_{n}\to f`$ fast gleichmäßig $`\implies f_{n}\to f`$ dem Maße
nach (bzw. stochastisch oder in Wahrscheinlichkeit).

Der andere Weg von der gleichmäßigen Konvergenz zur Konvergenz dem
Maße nach führt über die $`L^{p}`$-Konvergenz:

* $`f_{n}\to f`$ gleichmäßig $`\implies f_{n}\to f`$ in $L^{\infty}$.
* $`f_{n}\to f$ in $L^{\infty}\implies f_{n}\to f`$ in $L^{p}$ für alle reellen $0<p<\infty$.
* $`\boldsymbol{f}_{n}\to\boldsymbol{f}`$ in
$`\boldsymbol{L}^{p}\implies\boldsymbol{f}_{n}\to\boldsymbol{f}`$ in
$`\boldsymbol{L}^{q}`$ für alle reellen $`\mathbf{0}<\boldsymbol{q}<\boldsymbol{p}`$.
* $`f_{n}\to f`$ in $`L^{p}`$ für $`0<p\leq\infty\implies f_{n}\to f$
dem Maße nach (bzw. stochastisch oder in Wahrscheinlichkeit).

Von der Konvergenz dem Maße nach gelangt man zur schwachen Konvergenz:

* $`f_{n}\to f`$ dem Maße nach $\implies f_{n}\to f$ schwach (bzw. in Verteilung).

## Wichtige Theoreme über Funktionenfolgen

* Satz von Arzelà-Ascoli
* Satz von Dini
* Satz von Jegorow
* Literatur
Heinz Bauer: Maß- und Integrationstheorie. 2. Auflage. De Gruyter, Berlin 1992, ISBN 3-11-
013626-0 (Gebunden), ISBN 3-11-013625-2 (Broschiert), ab S. 91 (§15 Konvergenzsätze) und

%TODO: Presentar algunos espacios.