## articles

### Source

* [Anti M-Weierstrass function sequences](https://arxiv.org/abs/2003.10263)
* [Funktionenfolge](https://de.wikipedia.org/wiki/Funktionenfolge)

### View document in web browser :globe_with_meridians:

* [`Anti_M-Weierstrass_function_sequences.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions-references/Anti_M-Weierstrass_function_sequences.pdf)
* [`Funktionenfolge.pdf`](https://courses-2020-1.gitlab.io/sequences-and-series-of-functions-references/Funktionenfolge.pdf)